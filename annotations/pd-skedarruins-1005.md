Floor switch

Room 5F is other side of ravine

Wait for:
- Object on switch -> 2D
- Player on switch -> 2D
- Player on other side of ravine -> 2E
- If none of those, unset flag 17

When crossed ravine, set flags 16 and 17