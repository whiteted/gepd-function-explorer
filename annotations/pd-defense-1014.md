Carrington messages

Wait for flag 26
If SA or PA:
- Wait 3 seconds
- "Joanna, we're under attack"
- Wait 3 seconds
- "Get the guns back online - hurry"
- Wait 3 seconds

Set flag 28 (trigger for Jo to speak?)

If SA or PA:
- Wait for objective 0 (autoguns) to complete

Hide Carrington and set invincible
Unlock 2 doors - probably hostage doors
Wait 3 seconds
"The Skedar have taken hostages"
Wait 3 seconds
"Get up to the offices and save them"
Wait 3 seconds
Set flag 29
Wait for objective 1 (hostages) completed
Wait 5 seconds
Wait for sound in channel 6 to finish
"They're using a new form of shield technology"
Wait 3 seconds
"Foster was working on a new weapon which may be useful"
Wait 3 seconds
Wait for objective 2 (rcp120) completed

If PA:
- Wait 3 seconds
- "Damn it, my office... if they get access"
- Wait 4 seconds
- "Get there first, Jo, and destroy the files"
- Wait 3 seconds
- Wait for objective 3 (safe) completed

Wait until Joanna/Velvet/Counter op not in certain rooms (skedar shuttle area)
Wait 3 seconds
"Things are desperate"
Wait 3 seconds
"Find it and get it out of the building"
Wait 3 seconds
Start X music
Start 2 minute countdown timer
Wait for flag 25 or timer expired

If timer expired:
- Set flag 24
- Hide timer

Wait 5 seconds
"Well done Joanna, we're nearly clear"
Wait 3 seconds
"The last dropship is waiting for you"
Wait 3 seconds
Wait for all objectives complete
Wait for player to enter room
Make Jo invincible and assign function 0418 to $this