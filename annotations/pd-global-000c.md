Combat with ally?

This function will assign function 0007 unless you are on one of these stages and some other condition is met:

* Escape
* Crash Site
* Deep Sea
* Defense
* Attack Ship
* WAR

Those levels are the only levels who contain allies (Jonathan, Elvis, CI guards) who can attack. Perhaps it allows the guard to see the ally and become alerted?

Also handles:

* injuries
* running to cover
* attacking
* possibly more
