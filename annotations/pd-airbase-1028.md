Activate lifts

If Agent:
- Hide some laser beam
Else (SA/PA):
- Unlock door leading to safe area
Endif

Deactivate lifts
Wait for security subverted
Activate lifts