Bot activation terminal

SA/PA only:
Wait for terminal to be activated
If objective is failed, show "Maintenance robot system offline"
Elseif objective is completed, show "Operation not allowed - robots busy"
Elseif bot is currently running, show "Maintenance robots deactivated" and unset flags 10, 30, 31
Elseif bot has program, show "Maintenance robots activated", "Maintenance cycle activated", set flag 10 (robots active) and complete objective
Else, show "Maintenance robots activated", set flags 10 (robots active) and 31

If you turn the bot on without programming it, it moves slowly in a short path. If you then program it while it's running, it starts the routine cleaning cycle where it moves up and down the room at medium speed. To program it for lasers, you must program it while the bot is off.