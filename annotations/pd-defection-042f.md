Surrendering guard

This is the first guard after you enter the building.

Wait for visual on Jo, detected Jo or nearby gunshot
"What the", "Who the", or "Who the"
Start surprised animation
Wait 1 second
Then 50% chance of one action or another

Action 1 (label 54):
Run to where Jo is at the moment
If guard got within 30 units of Jo and has line of sight to Jo, surrender and exit
If guard got there without coming within 30 units of Jo, alert friends
Proceed to label 55

Action 2 (label 55):
If Jo within 30 units, surrender and exit
Wait until player in line of sight
Wait 10 seconds then stop?