Jog to Bond and attack if seen

* Start jogging to Bond - if this fails (eg. no path to Bond) then jump to return function
* Wait until not moving or Bond in sight
* If Bond in sight, assign 0006
* If reached target without seeing Bond, jump to return function
