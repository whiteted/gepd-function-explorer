Limo

Wait for flag 19
Wait until Jo not in certain room
Fly away

The objective fails as soon as the limo starts to move, which is presumably when you're not near it. What happens if you throw the bug on it while it's airborne?