Hanger

Wait for:
    If flag 29 is true, goto 2C
End wait

[Label 2C]
Reset and start cycle counter
Wait for:
    If cycle counter > 7200 (120 secs), goto 2C
    If difficulty < SA:
        If civilians killed > 5, goto 2C
    Else if difficulty < PA:
        If civilians killed > 7, goto 2C
    Else
        If civilians killed > 9, goto 2C
    End if
End wait

[Label 2C]
Set $flag_16
...