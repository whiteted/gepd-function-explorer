Check hostages saved

Interesting things here:
- Objective won't complete until either all takers or all hostages from each room have died
- Checking order is firing range, non-RNG room, RNG room, hologram room
- If flag 13 is set (hostage objective failed?), the hostages saved message won't appear
- Flag 12 = hostages saved

02 = CI Male
07 = CI Male
1F = ST
20 = ST
01 = CI Female
06 = CI Male
03 = CI Male
08 = CI Female
04 = CI Female

No flag = firing range takers (1F/20) dead - hostages are 02/07
Flag 19 = hologram takers (21/22/23) dead
Flag 20 = two upstairs takers (24/25) dead
Flag 21 = two upstairs takers (26/27) dead - hostages are 03/08 (room with 50/50 random)


0A:
If firing range hostage 1 dead goto 2F
Goto next 2E

2F:
If firing range hostage 2 dead goto 2F

2E:
If firing range taker 1 dead goto 2E
Goto first 0A

2E:
If firing range taker 2 dead goto 2E
Goto first 0A

(ie. wait for both firing range hostages to be dead OR hostage 1 alive and taker 1 dead)

2F:
2E:
If flag 20 (upstairs non-random room's takers are dead) goto 2E
If CI female dead goto 2F
Goto first 0A

2F:
If CI male dead goto 2E
Go to first 0A

(ie. wait for both takers dead, or both hostages dead)

2E:
If flag 21 goto 2E
If CI male dead goto 2F
Goto first 0A

2F:
If CI female dead goto 2E
Goto first 0A

(if both RNG room takers dead, proceed)
(if both RNG room hostages dead, proceed)

2E:
If flag 19 goto 2E
If CI female dead goto 2E
Goto first 0A

(if hologram takers dead, proceed)
(if hologram hostage dead, proceed)

2E:
If flag 13 exit
Show hostages saved message
Set flag 12
Exit