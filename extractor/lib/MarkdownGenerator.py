import lib.GeRegistry
import lib.PdRegistry
import os, shutil

def load(game):
    if game == 'ge':
        return GeGenerator()
    else:
        return PdGenerator()

class Generator:
    functions = None
    anno_dir = None
    output_dir = None

    def setFunctions(self, functions):
        self.functions = functions
        return self

    def setAnnotationsDirectory(self, dir):
        self.anno_dir = dir
        return self;

    def setOutputDirectory(self, dir):
        self.output_dir = dir
        return self;

    def generate(self):
        if os.path.isdir(self.output_dir):
            shutil.rmtree(self.output_dir)
        os.mkdir(self.output_dir)
        for index, name in enumerate(self.STAGES):
            os.mkdir("%s/%02d-%s" % (self.output_dir, index, name))
        for function in self.functions:
            self.annotate(function)
        for function in self.functions:
            self.generateFile(function)

    def annotate(self, function):
        filename = '%s/%s-%02d-%04x.md' % (self.anno_dir, self.GAME, function['stage_id'], function['id'])
        # Defaults
        function['title'] = '%04x' % function['id']
        function['filename'] = '%04x.md' % function['id']
        function['comments'] = None
        try:
            fp = open(filename, 'r')
        except:
            return
        title = fp.readline().strip()
        if title: function['title'] = '%04x - %s' % (function['id'], title)
        function['filename'] = '%s.md' % function['title'].replace('/', ' or ')
        function['comments'] = ''.join(fp.readlines()).strip()
        fp.close()

    def generateFile(self, function):
        filename = "%s/%02d-%s/%s" % (self.output_dir, function['stage_id'], self.STAGES[function['stage_id']], function['filename'])

        fp = open(filename, 'w')
        self.writeTitle(fp, function)
        self.writeComments(fp, function)
        self.writeInvocations(fp, function)
        self.writeFunction(fp, function)
        fp.close()

    def writeTitle(self, fp, function):
        fp.write('# %s\n\n' % function['title'])

    def writeComments(self, fp, function):
        if function['comments']:
            fp.write('## Comments\n\n')
            fp.write('%s\n\n' % function['comments'])

    def writeInvocations(self, fp, function):
        fp.write('## Invocations\n\n')
        if function['id'] & 0xff00 == 0x0000:
            fp.write("This function may be invoked from any other function in the game.\n\n")
            return
        invocations = []
        invocations += self.findInvocationsAuto(function);
        invocations += self.findInvocationsInitialActors(function);
        invocations += self.findInvocationsInitialObjects(function);
        invocations += self.findInvocationsAssigned(function);
        if len(invocations):
            for invocation in invocations:
                fp.write('* %s\n' % invocation)
            fp.write('\n')
        else:
            fp.write("This function doesn't seem to be invoked anywhere.\n\n")

    def findInvocationsAuto(self, function):
        if function['id'] & 0xff00 == 0x1000:
            return ['Started automatically']
        if function['id'] & 0xff00 == 0x1400:
            return ['Started automatically']
        if function['id'] & 0xff00 == 0x0c00:
            return ['Started automatically when using cinema menu']
        return []

    def findInvocationsInitialActors(self, function):
        if not function['stage_id'] in self.registry.stage_specific:
            return []
        invocations = []
        actors = self.registry.stage_specific[function['stage_id']]['actors']
        for actor_index in actors:
            actor = actors[actor_index]
            if actor['function'] == function['id']:
                invocations.append('Initial function for actor %02x (%s)' % (actor_index, actor['name']))
        return invocations

    def findInvocationsInitialObjects(self, function):
        if not function['stage_id'] in self.registry.stage_specific:
            return []
        invocations = []
        objects = self.registry.stage_specific[function['stage_id']]['objects']
        for object_index in objects:
            obj = objects[object_index]
            if 'function' in obj and obj['function'] == function['id']:
                invocations.append('Initial function for object %02x (%s)' % (object_index, obj['name']))
        return invocations

    def findInvocationsAssigned(self, function):
        invocations = []
        text = '[%04x]' % function['id']
        for func in self.functions:
            if function['stage_id'] != func['stage_id']:
                continue
            if function['id'] == func['id']:
                continue
            for instruction in func['instructions']:
                if text in instruction['description']:
                    invocations.append('Can be invoked by function [%s](%s)' % (func['title'], func['filename']))
                    break
        return invocations

    def writeFunction(self, fp, function):
        code_len = len('Code')
        desc_len = len('Description')
        rows = []
        for instruction in function['instructions']:
            data_string = '`' + self.typeToHexString(instruction['type'])
            for char in instruction['params']:
                data_string += '%02x' % char
            data_string += '`'
            description = self.markupDescription(function, instruction['description'])
            code_len = max(code_len, len(data_string))
            desc_len = max(desc_len, len(description))
            rows.append([data_string, description])

        fp.write('## Function\n\n')
        fp.write('| %s | %s |\n' % ('Code'.ljust(code_len), 'Description'.ljust(desc_len)))
        fp.write('| %s | %s |\n' % (''.ljust(code_len, '-'), ''.ljust(desc_len, '-')))
        for row in rows:
            fp.write('| %s | %s |\n' % (row[0].ljust(code_len), row[1].ljust(desc_len)))
        fp.write('\n')

    def markupDescription(self, function, description):
        for func in self.functions:
            if func['stage_id'] != function['stage_id'] and func['stage_id'] != 0:
                continue
            description = description.replace('[%04x]' % func['id'], '[%s]' % func['title'])
            description = description.replace('(%04x.md)' % func['id'], '(%s)' % func['filename'])
            description = description.replace('/%04x.md)' % func['id'], '/%s)' % func['filename'])
        description = description.replace('\n', '<br>')
        return description

class GeGenerator(Generator):
    GAME = 'ge'
    STAGES = ['global','dam','facility','runway','surface1','bunker1','silo','frigate','surface2','bunker2','statue','archives','streets','depot','train','jungle','control','caverns','cradle','aztec','egypt','credits']
    registry = lib.GeRegistry

    def typeToHexString(self, type_id):
        return '%02x' % type_id

class PdGenerator(Generator):
    GAME = 'pd'
    STAGES = ['global','defection','investigation','extraction','villa','chicago','g5building','infiltration','rescue','escape','airbase','airforceone','crashsite','pelagic2','deepsea','defense','attackship','skedarruins','mbr','maiansos','war','duel','citraining','skedarmp']
    registry = lib.PdRegistry

    def typeToHexString(self, type_id):
        return '%04x' % type_id
